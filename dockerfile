FROM microsoft/aspnetcore:1.1

EXPOSE 5000

RUN mkdir -p /opt/app-root/src /opt/app-root/publish && \
    useradd -u 1001 -r -g 0 -d /opt/app-root/src -s /sbin/nologin \
      -c "Default Application User" default && \
    chown -R 1001:0 /opt/app-root && chmod -R og+rwx /opt/app-root
RUN chmod -R 775 /usr/share/dotnet/
USER 1001
WORKDIR /opt/app-root/src

COPY /bin/release/netcoreapp1.1/publish/ .
ENTRYPOINT ["dotnet", "hellodotnetcore.dll"]